import React, { Component } from 'react';
import "./index.css"
import "./normalize.css"

import { connect } from 'react-redux'

import {
  attemptWidgetCreation,
  checkOrderForPackaging,
  generateOrder,
  orderMaterials,
  shipOrder
} from './actions'

import Inventory from './Inventory'
import Materials from './Materials'
import Management from './Management'
import Manufacturing from './Manufacturing'
import Orders from './Orders'
import QA from './QA'
import Packaging from './Packaging'
import Shipping from './Shipping'

const Header = () => <img className="logo" src="/logo.png" />
const Error = (props) => <h4>{props.message}</h4>

class App extends Component {
  render() {
    const {
      attemptWidgetCreation,
      checkOrderForPackaging,
      error,
      failed,
      generateOrder,
      materials,
      orderMaterials,
      orders,
      packaged,
      shipped,
      shipOrder,
      widgets
    } = this.props

    return (
      <div>
        <Header />
        {error ? <Error message={error} /> : null}
        <Orders generateOrder={generateOrder} orders={orders} />
        <Materials materials={materials} />
        <Manufacturing
          addWidget={attemptWidgetCreation}
          materials={materials}
        />
        <QA failed={failed} />
        <Inventory widgets={widgets} />
        <Packaging orders={orders} packaged={packaged} packageOrder={checkOrderForPackaging} />
        <Shipping packaged={packaged} shipped={shipped} shipOrder={shipOrder} />
        <Management orderMaterials={orderMaterials} />
      </div>
    );
  }
}

const mapStateToProps = (state) => state
const mapActionCreators = {
  attemptWidgetCreation,
  checkOrderForPackaging,
  generateOrder,
  orderMaterials,
  shipOrder
}

export default connect(
  mapStateToProps,
  mapActionCreators
)(App);

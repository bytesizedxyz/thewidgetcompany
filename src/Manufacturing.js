import React, { Component } from 'react';

class Manufacturing extends Component {
  render() {
    return (
      <div>
        <h2>Manufacturing</h2>
        <button onClick={this.props.addWidget}>Manufacture widget</button>
      </div>
    );
  }
}

export default Manufacturing;

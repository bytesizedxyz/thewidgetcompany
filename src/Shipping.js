import React, { Component } from 'react';

class Shipping extends Component {
  render() {
    const { packaged, shipped, shipOrder } = this.props
    return (
      <div>
        <h2>Shipping</h2>
        <h4>{shipped} orders shipped</h4>
        {packaged ? <button onClick={shipOrder}>Ship order</button> : <h4>No orders to ship</h4>}
      </div>
    );
  }
}

export default Shipping;

import {
  ADD_WIDGET,
  GENERATE_ORDER,
  ORDER_MATERIALS,
  PACKAGE_ORDER,
  PRESENT_ERROR,
  SHIP_ORDER,

  DOWELS_NEEDED,
  SCREWS_NEEDED,
  WHEELS_NEEDED
} from './constants'

export function attemptWidgetCreation() {
  return (dispatch, getState) => {
    const { materials } = getState()
    const { dowel, screw, wheel } = materials

    if (dowel.count >= DOWELS_NEEDED && screw.count >= SCREWS_NEEDED && wheel.count >= WHEELS_NEEDED) {
      dispatch(addWidget())
    } else {
      dispatch(presentError("Not enough materials to create a widget"))
    }
  }
}

export function addWidget() {
  return {
    date: Date.now(),
    type: ADD_WIDGET
  }
}

export function generateOrder() {
  return {
    date: Date.now(),
    type: GENERATE_ORDER
  }
}

export function orderMaterials() {
  return {
    type: ORDER_MATERIALS
  }
}

export function checkOrderForPackaging() {
  return (dispatch, getState) => {
    const { orders, widgets } = getState()
    const order = orders[0]
    if (order.widgets <= widgets.length) {
      dispatch(packageOrder(order))
    } else {
      dispatch(presentError("Not enough widgets to fill order!"))
    }
  }
}

export function packageOrder(order) {
  return {
    type: PACKAGE_ORDER,
    order
  }
}

export function shipOrder() {
  return {
    type: SHIP_ORDER
  }
}

export function presentError(message) {
  return {
    type: PRESENT_ERROR,
    message
  }
}

export default {
  attemptWidgetCreation,
  generateOrder,
  orderMaterials,
  packageOrder,
  presentError,
  shipOrder
}
